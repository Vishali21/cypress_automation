import { data } from '../env/config'

class dNotePage {


    deliveryNoteAdd = '#DeliveryNote > .fa'

    salesOrder = '#dropdownlistArrowdelivery_salesorder_salesorderdefs_id > .jqx-icon-arrow-down'

    salesOrderDropdown = '/html/body/div[15]/div/div/div/div[2]/div/div[11]/span'

    approved = '#dropdownlistContentdelivery_defs_approved > .jqx-combobox-input'

    memo = '#delivery_defs_memo'

    customerJob = '#dropdownlistContentdelivery_defs_customer_id > .jqx-combobox-input'

    customerLocation = '#delivery_defs_customer_location'

    salesPerson = '#dropdownlistContentdelivery_defs_employeemaster_id > .jqx-combobox-input'

    customerBranch = '#dropdownlistContentdelivery_defs_customerbranches_id > .jqx-combobox-input'

    customerPo = '#delivery_defs_customer_po'

    financialCenters = '#dropdownlistContentdelivery_defs_finareas_id > .jqx-combobox-input'

    inventoryLocation = '#dropdownlistContentdelivery_defs_vallocations_id > .jqx-combobox-input'

    addItems = '#formGrid0BulkSelectBtn > span'

    description1 = '/html/body/div[7]/div/section/div/article/div/div/div[2]/div[2]/form/div[1]/fieldset[3]/div[4]/div/div/div/div[2]/div/div/div/div/div[4]/div[2]/div/div[1]/div[4]'

    description2 = '/html/body/div[7]/div/section/div/article/div/div/div[2]/div[2]/form/div[1]/fieldset[3]/div[4]/div/div/div/div[2]/div/div/div/div/div[4]/div[2]/div/div[2]/div[4]'

    cancel = '#modalCancelBulkSelect0'

    location = '[style="left: 444px; z-index: 91; width: 116px;"] > .jqx-grid-cell-left-align'

    clickInventoryLocation = '#dropdownlistArrowdelivery_defs_vallocations_id > .jqx-icon-arrow-down'

    clickInventoryLocationDropdown = '/html/body/div[26]/div/div/div/div[2]/div/div[1]/span'



    deliveryNote() {
        cy.clickElement(this.deliveryNoteAdd)
    }

    deliveryDetails() {
        cy.clickElement(this.salesOrder)
        cy.wait(3000)
        cy.xpathElement(this.salesOrderDropdown).wait(4000)
        cy.get(this.approved).should('have.value', data.approved)
        cy.get(this.memo).should('have.value', data.memo).wait(2000)
    }

    customerDetails() {
        cy.get(this.customerJob).should('have.value', data.customerJob)
        cy.get(this.customerLocation).should('have.value', data.customerLocation).wait(2000)
        cy.get(this.salesPerson).should('have.value', data.salesPerson)
        cy.get(this.customerBranch).should('have.value', data.customerBranch).wait(2000)
        cy.get(this.financialCenters).should('have.value', data.financialCenters)
        cy.get(this.customerPo).should('have.value', data.customerPo).wait(2000)
    }

    itemDetails() {
        cy.get(this.inventoryLocation).should('have.value', data.inventoryLocation)
        cy.get(this.location).should('contain', data.location)
        cy.clickElement(this.addItems)
        cy.wait(3000)
        cy.xpath(this.description1).should('contain', data.description1)
        cy.xpath(this.description2).should('contain', data.description2).wait(2000)
        cy.clickElement(this.cancel)
        cy.wait(2000)
    }

    selectInventoryLocation() {
        cy.clickElement(this.clickInventoryLocation)
        cy.wait(3000)
        cy.xpathElement(this.clickInventoryLocationDropdown)
    }

}

export default new dNotePage();