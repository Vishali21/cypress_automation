class PInvoicePage {

    btnPurchaseInvoice = '#PurchaseInvoice_id'

    purchaseInvoiceAdd = '#PurchaseInvoice > .fa'

    supplier = '#dropdownlistArrowpurchase_invoice_defs_supplier_id > .jqx-icon-arrow-down'

    supplierDropdown = '/html/body/div[15]/div/div/div/div[2]/div/div[1]/span/div'

    approved = '#dropdownlistArrowpurchase_invoice_defs_approved > .jqx-icon-arrow-down'

    approvedDropdown = '/html/body/div[17]/div/div/div/div[2]/div/div[1]/span'

    Po = '#dropdownlistArrowpurchaseinvoice_purchaseorder_purchaseorderdefs_id > .jqx-icon-arrow-down'

    poDropdown = '/html/body/div[18]/div/div/div/div[2]/div/div[1]/span'

    poDropdown1 = '/html/body/div[18]/div/div/div/div[2]/div/div[2]/span'


    purchaseInvoice() {
        cy.clickElement(this.purchaseInvoiceAdd)
    }

    verifyBtnPurchaseInvoice() {
        cy.clickElement(this.btnPurchaseInvoice).wait(3000)
    }

    url() {
        cy.visitUrlWithQueryParam('/admin/forms/loadForm/315', '?openInTab=current');
    }

    selectSupplier() {
        cy.wait(3000)
        cy.clickElement(this.supplier)
        cy.wait(3000)
        cy.xpathElement(this.supplierDropdown)
        cy.wait(4000)
        cy.clickElement(this.approved).wait(2000)
        cy.xpathElement(this.approvedDropdown).wait(2000)
    }

    selectPurchase() {
        cy.clickElement(this.Po)
        cy.wait(2000)
        cy.xpathElement(this.poDropdown, { timeout: 8000 }).wait(3000)

        // cy.clickElement(this.Po)
        // cy.wait(2000)
        // cy.xpathElement(this.poDropdown1, { timeout:8000 }).wait(3000)
    }



}

export default new PInvoicePage();
