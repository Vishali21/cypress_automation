class ItemRPage {

    btnItemReceipts = '#ItemReceipts_id'

    itemReceiptsAdd = '#ItemReceipts > .fa'

    purchaseOrder = '#dropdownlistArrowitem_receipts_defs_purchaseorderdefs_id > .jqx-icon-arrow-down'

    purchaseOrderDropdown = "/html/body/div[15]/div/div/div/div[2]/div/div[1]/span"

    purchaseOrder1 = '#dropdownlistContentitem_receipts_defs_purchaseorderdefs_id > .jqx-combobox-input'

    purchaseOrderDropdown1 = '/html/body/div[15]/div/div/div/div[2]/div/div/span'


    url() {
        cy.visitUrlWithQueryParam('/admin/forms/loadForm/140', '?openInTab=current');
    }

    verifyBtnItemReceipts() {
        cy.clickElement(this.btnItemReceipts).wait(3000)
    }

    itemReceipts() {
        cy.get(this.itemReceiptsAdd).click()
    }

    selectPurchaseOrder() {
        cy.clickElement(this.purchaseOrder)
        cy.wait(3000)
        cy.xpathElement(this.purchaseOrderDropdown).wait(2000)
    }

    typePurchaseOrder() {
        cy.get(this.purchaseOrder1, { timeout: 8000 }).wait(3000).clear().type('PO-000409')
        cy.xpathElement(this.purchaseOrderDropdown1, { timeout: 8000 }).wait(3000)
    }
}

export default new ItemRPage();