import {data} from '../env/config'

class quotationPage {

    salesIcon = ':nth-child(3) > [href="#"] > .collapse-sign > .fa'

    quotationAdd = '#Quotation > .fa'

    copyQuotation = '#dropdownlistArrowquotation_defs_quotationdefs_id > .jqx-icon-arrow-down'

    copyQuotationDropdown = '/html/body/div[15]/div/div/div/div[2]/div/div[1]/span'

    email = '#quotation_defs_email'

    itemName = '.jqx-grid-cell-hover > .jqx-grid-cell-left-align'
    


    sales() {
        cy.clickElement(this.salesIcon)
    }

    quotation() {
        cy.clickElement(this.quotationAdd)
    }

    copy() {
        cy.clickElement(this.copyQuotation).wait(2000)
        cy.xpath(this.copyQuotationDropdown, { timeout: 8000 }).click({ force: true }).wait(2000)
    }

    customerDetails(){
        cy.get(this.email).should('have.value', data.email)
    }

}

export default new quotationPage();