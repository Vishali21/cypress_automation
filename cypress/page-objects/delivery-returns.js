import { data } from '../env/config'

class dReturnPage {

    deliveryReturnsAdd = '#DeliveryReturns > .fa'

    deliveryNote = '#dropdownlistArrowdelivery_returns_defs_deliverydefs_id > .jqx-icon-arrow-down'

    deliveryNoteDropdown = '/html/body/div[15]/div/div/div/div[2]/div/div[10]/span'

    customerBranch = '#dropdownlistContentdelivery_returns_defs_customerbranches_id > .jqx-combobox-input'

    salesPerson = '#dropdownlistContentdelivery_returns_defs_employeemaster_id > .jqx-combobox-input'

    customerJob = '#dropdownlistContentdelivery_returns_defs_customer_id > .jqx-combobox-input'

    returnTo = '#dropdownlistContentdelivery_returns_defs_vallocations_id > .jqx-combobox-input'

    returnToDropdown = '/html/body/div[22]/div/div/div/div[2]/div/div[3]/span'


    deliveryReturn() {
        cy.clickElement(this.deliveryReturnsAdd)
    }

    selectDelivery() {
        cy.clickElement(this.deliveryNote)
        cy.wait(3000)
        cy.xpathElement(this.deliveryNoteDropdown)
        cy.wait(2000)
        cy.get(this.customerBranch).should('have.value', data.customerBranch)
        cy.get(this.salesPerson).should('have.value', data.salesPerson).wait(2000)
        cy.get(this.customerJob).should('have.value', data.customerJob1)
    }

    selectItems() {
        cy.clickElement(this.returnTo)
        cy.wait(2000)
        cy.xpathElement(this.returnToDropdown)
    }

}

export default new dReturnPage();