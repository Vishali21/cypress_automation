class PRFQPage {

    procurementIcon = ':nth-child(6) > [href="#"] > .collapse-sign > .fa'

    addPurchaseRFQ = '#PurchaseRFQ > .fa'

    purchaseRequest = '#dropdownlistArrowpurchase_rfq_defs_purchaseintentdefs_id > .jqx-icon-arrow-down'

    dropDown = '/html/body/div[15]/div/div/div/div[2]/div/div[7]/span'

    supplier = '#purchaserfq_supplier_supplier_idBtn > [style="display: inline; position: absolute; left: 4px; top: 4px;"] > .fa'

    frame = '#popUpFormContainerpurchaserfq_supplier_supplier_id'

    name = '#supplier_name'

    altAR = '#supplier_name_alt'

    addressLine = "#supplier_details_address_line_1"

    city = "#supplier_details_city"

    addItems = "div[id='formGrid0BulkSelectBtn'] span"

    cancel = "#modalCancelBulkSelect0"

    active = '#dropdownlistArrowsupplier_active'

    dropdown1 = "//span[contains(text(),'YES')]"


    procurementModule() {
        cy.clickElement(this.procurementIcon)
        cy.clickElement(this.addPurchaseRFQ)
    }

    createPurchaseRFQ() {
        cy.clickElement(this.purchaseRequest)
        cy.wait(2000)
        cy.xpathElement(this.dropDown)
    }

    purchaseRFQStatus() {
        cy.wait(3000)
        cy.clickElement(this.supplier)
        cy.wait(5000)
    }

    enterSupplierDetails() {
        cy.get(this.frame).its('0.contentDocument.body').find(this.name).type('Vishali')
        cy.get(this.frame).its('0.contentDocument.body').find(this.altAR).type('Translate').wait(2000)
        cy.get(this.frame).its('0.contentDocument.body').find("input[value='REGISTERED']").clear().type('AGENT')
        cy.wait(2000)
        cy.get(this.frame).its('0.contentDocument.body').find("#ENTERSUPPLIERPREFERENCESLegend").click()
    }

    enterAddressDetails() {
        cy.get(this.frame).its('0.contentDocument.body').find(this.addressLine).type('Chennai')
        cy.get(this.frame).its('0.contentDocument.body').find(this.city).type('Madhavaram')
        cy.wait(2000)
    }

    enterContactDetails() {
        cy.get(this.frame).its('0.contentDocument.body').find(this.addItems).click({ force: true })
        cy.wait(3000)
        cy.get(this.frame).its('0.contentDocument.body').find(this.cancel).click({ force: true })

    }

}

export default new PRFQPage