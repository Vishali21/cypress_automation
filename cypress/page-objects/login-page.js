class LoginPage {

    get userNameInput() { return cy.get("#username") }

    get passwordInput() { return cy.get("#password") }

    get loginButton() { return cy.get(".bg-white > .btn") }

    get errorMessage() { return cy.get('.fw-400 > .m-0') }

    get notification() { return cy.get('#smallbox1') }

    get clickClose() { return cy.get(".modal-header > .close") }

    get sideOpen() { return cy.get("#hide-menu > span > a > .fa") }

    

    visit() {
        return cy.visit('/')
    }
}

export default new LoginPage();
