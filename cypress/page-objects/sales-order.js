class SOPage {


    salesOrderAdd = '#SalesOrder > .fa'

    quotation = '#dropdownlistArrowsales_order_defs_quotationdefs_id > .jqx-icon-arrow-down'

    quotationDropdown = '/html/body/div[15]/div/div/div/div[2]/div/div[1]/span'


    salesOrder() {
        cy.clickElement(this.salesOrder)
    }

    selectQuotation() {
        cy.clickElement(this.quotation)
        cy.wait(3000)
        cy.xpath(this.quotationDropdown, { timeout: 8000 }).click({ force: true }).wait(2000)
    }

}

export default new SOPage();