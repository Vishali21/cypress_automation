import { data } from '../env/config'

class Payment {

    income = ':nth-child(9) > [href="#"] > .collapse-sign > .fa'

    payment = '#ReceivePayment > .fa'

    dropdown = '#dropdownlistArrowpayment_defs_customer_id > .jqx-icon-arrow-down'

    list = '/html/body/div[15]/div/div/div/div[2]/div/div[1]'

    checkbox = '/html/body/div[7]/div/section/div/article/div/div/div[2]/div[2]/form/div[1]/fieldset[5]/div[3]/div/div/div/div/div/div[4]/div[2]/div/div[1]/div[1]/div/div[1]/div/span'

    customer = '#CUSTOMERLabel > strong'

    availableAdvance = '#AVAILABLE_ADVANCELabel > strong'

    dateToSelect = '4/29/2023'

    date = '[style="height: 100%; width: 22px; left: 432px;"] > .jqx-icon'

    visible = '/html/body/div[16]/div[1]'

    year = '/html/body/div[16]/div[1]/div/div[1]/table/tbody/tr/td[2]/div'

    month = '/html/body/div[16]/div[1]/div/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[4]'

    date1 = '/html/body/div[16]/div[1]/div/table/tbody/tr[2]/td[2]/table/tbody/tr[1]/td[6]'

    amount = '#payment_defs_total > .jqx-input-content'


    clickIncome() {
        cy.clickElement(this.income)
        cy.clickElement(this.payment)
    }

    selectCustomer() {
        cy.clickElement(this.dropdown)
        cy.wait(2000)
        cy.xpath(this.list, { timeout: 8000 }).click({ force: true })
        cy.get(this.customer).should('have.text', data.customer)
        cy.get(this.availableAdvance).should('have.text', data.availableAdvance)
    }

    selectPaymentMethod() {
        cy.clickElement(this.date)
        cy.wait(10000)
        cy.xpathElement(this.year)
        cy.wait(3000)
        cy.xpathElement(this.month)
        cy.xpathElement(this.date1)
    }

    clickCheckbox() {
        cy.xpathElement(this.checkbox)
    }

    receivePayment() {
        cy.get(this.amount).should('have.value', data.amount)
    }


}
export default new Payment;