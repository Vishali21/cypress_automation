class BillPayments {

    makePayment = '#PayBills_id'

    payfrom = '#dropdownlistArrowbill_defs_finaccounts_id > .jqx-icon-arrow-down'

    dropdown = '/html/body/div[17]/div/div/div/div[2]/div/div[1]/span'

    total = '/html/body/div[7]/div/section/div/article/div/div/div[2]/div[2]/form/div[1]/fieldset[4]/div[1]/div[1]/div/div/input'



    clickPayment() {
        cy.wait(5000)
        // cy.get(this.makePayment).invoke('attr', 'href').then(href => {
        // cy.visit('https://issam.halerp.com/admin/forms/loadForm/153?supplier_ref=20__Siva%20Balan')
        cy.visit('https://issam.halerp.com/admin/forms/loadForm/153?supplier_ref=10__test%20foreign')
        // })
    }

    verifyPayment() {
        cy.clickElement(this.makePayment)
    }

    selectAccountToPay() {
        cy.clickElement(this.payfrom)
        cy.wait(3000)
        cy.xpathElement(this.dropdown)
    }

    applyPayments() {
        cy.wait(3000)
        cy.xpath(this.total).wait(3000).dblclick({ force: true })
        cy.xpath(this.total).wait(2000).type('600.88')
    }

}

export default new BillPayments;