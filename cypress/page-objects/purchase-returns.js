import { data } from '../env/config'

class PReturnPage {

    addPurchaseReturns = '#PurchaseReturns > .fa'

    transactionType = '#dropdownlistArrowpurchase_returns_defs_valtranstypes_id > .jqx-icon-arrow-down'

    dropDown = '/html/body/div[15]/div/div/div/div[2]/div/div[1]/span'

    transactionRef = '#dropdownlistArrowpurchase_returns_defs_type_defs_id > .jqx-icon-arrow-down'

    dropDown1 = '/html/body/div[16]/div/div/div/div[2]/div/div[4]/span/div/div[1]'

    memo = '#purchase_returns_defs_memo'

    quantityToReturn = '[style="left: 964px; z-index: 69; width: 123px;"] > .jqx-grid-cell-right-align'

    quantityToReturn1 = '#numbereditorformGrid0purchase_returns_items_qty > .jqx-input-content'


    procurementModule() {
        cy.get(this.addPurchaseReturns).click({ force: true })
    }

    selectTheTransaction() {
        cy.get(this.transactionType).click()
        cy.wait(2000)
        cy.xpath(this.dropDown).click({ force: true })
        cy.get(this.transactionRef).click()
        cy.wait(2000)
        cy.xpath(this.dropDown1).click({ force: true })
    }

    returnDetails() {
        cy.wait(2000)
        cy.get(this.memo).type(data.memo1)
    }

    selectItems() {
        cy.wait(3000)
        cy.get(this.quantityToReturn)
            .dblclick()
        cy.wait(5000)
        cy.get(this.quantityToReturn1).type(data.qualityToReturn)

    }
}

export default new PReturnPage();