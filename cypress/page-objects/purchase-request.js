import { data } from '../env/config'

class PRPage {

    procurementIcon = ':nth-child(6) > [href="#"] > .collapse-sign > .fa'

    purchaseRequestAdd = '#PurchaseRequest > .fa'

    salesOrderClose = '[style="position: relative; overflow: hidden; float: right; min-height: 16px; min-width: 18px;"] > .jqx-icon-close'

    salesOrderDropdownIcon = '#dropdownlistArrowpurchaseintent_salesorder_salesorderdefs_id > .jqx-icon-arrow-down'

    typeSalesOrder = "/html/body/div[7]/div/section/div/article/div/div/div[2]/div[2]/form/div[1]/fieldset[1]/div[2]/div[1]/div/div/div/div/div[1]/input"

    project = "/html/body/div[7]/div/section/div/article/div/div/div[2]/div[2]/form/div[1]/fieldset[1]/div[2]/div[2]/div/input"

    click480 = "//b[contains(text(),'SO-00480')]"

    click003 = "//b[contains(text(),'SO-00003')]"

    customerLocation = "/html/body/div[7]/div/section/div/article/div/div/div[2]/div[2]/form/div[1]/fieldset[1]/div[3]/div[2]/div/input"

    customerPO = '#purchase_intent_defs_customer_po'

    memo = '#purchase_intent_defs_memo'

    reason = '#purchase_intent_defs_reason'

    customerJob = "//input[@value='HOLIDAY INN']"

    requestedBy = '#dropdownlistArrowpurchase_intent_defs_employeemaster_id > .jqx-icon-arrow-down'

    requestedByDropdown = "/html/body/div[17]/div/div/div/div[2]/div/div[3]/span"

    requestedByDropdown1 = "/html/body/div[17]/div/div/div/div[2]/div/div[2]/span"

    requestedByInput = '//*[@id="dropdownlistContentpurchase_intent_defs_employeemaster_id"]/input'

    add = '#FormAdd'

    view = '#lookupLink > :nth-child(3)'


    verifyPurchaseRequestAdd() {
        cy.get(this.purchaseRequestAdd).click({ force: true })
        cy.wait(2000)
    }

    createFromSalesOrder1() {

        cy.get(this.salesOrderClose).click({ force: true })
        cy.xpath(this.typeSalesOrder).type(data.typeSalesOrder)
        cy.wait(2000)
        cy.xpath(this.click480).click({ force: true })
        cy.wait(2000)
        cy.xpath(this.project).type(data.project)
        cy.wait(2000)
        cy.xpath(this.customerLocation).type(data.customerLocation)
        cy.wait(2000)
        cy.get(this.customerPO).type(data.customerPo)
        cy.wait(2000)
    }

    purchaseRequester1() {
        cy.get(this.requestedBy).click({ force: true })
        cy.wait(2000)
        cy.xpath(this.requestedByDropdown1).click({ force: true })
        cy.wait(2000)
    }

    purchaseRequests() {
        cy.get(this.memo).type(data.memo1)
        cy.wait(2000)
        cy.get(this.reason).should('have.value', data.reason)
    }

    purchaseRequest() {
        cy.clickElement(this.procurementIcon)
    }

    url() {
        cy.visitUrlWithQueryParam('/admin/forms/loadForm/159', '?openInTab=current');
    }

    createFromSalesOrder() {
        cy.clickElement(this.salesOrderClose)
        cy.xpath(this.typeSalesOrder).type(data.typeSalesOrder1)
        cy.xpathElement(this.click003)
        cy.scrollTo(0, 1400).wait(6000)
    }

    purchaseRequests1() {
        cy.get(this.reason).should('have.value', data.customerJob2)
        cy.wait(2000)
    }

    costingDetails() {
        cy.xpath(this.customerJob).should('have.value', data.costingDetails)
        cy.wait(7000)
    }

    purchaseRequester() {
        cy.wait(3000)
        cy.xpath(this.requestedByInput).clear().type(data.requestBy)
        cy.wait(3000)
        cy.xpathElement(this.requestedByDropdown1).wait(3000)
    }

    clickAdd() {
        cy.clickElement(this.add)
        cy.wait(10000)
    }

    verifyView() {
        cy.clickElement(this.view)
        cy.wait(10000)
    }


}

export default new PRPage();
