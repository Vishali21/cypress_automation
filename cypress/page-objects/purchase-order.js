import { data } from '../env/config'


class POrderPage {

    procurementIcon = ':nth-child(6) > [href="#"] > .collapse-sign > .fa'

    clickPurchaseOrder = '#PurchaseOrder > .fa'

    btnPurchaseOrder = '#PurchaseOrder_id'

    currentStatus = '#dropdownlistArrowpurchase_order_defs_current_status > .jqx-icon-arrow-down'

    currentStatusDropdown = "//span[contains(text(),'PICKED FROM MANUFACTURER')]"

    supplier = "/html/body/div[7]/div/section/div/article/div/div/div[2]/div[2]/form/div[1]/fieldset[3]/div[1]/div[1]/div/div[1]/div/div/div/div[2]/div"

    supplierDropdown = '/html/body/div[22]/div/div/div/div[2]/div/div[1]/span'

    customerJob = ' /html/body/div[7]/div/section/div/article/div/div/div[2]/div[2]/form/div[1]/fieldset[4]/div/div[1]/div/div[1]/div/div/div/div[1]/input'

    customerJobDropdown = '/html/body/div[28]/div/div/div/div[2]/div/div[1]/span'

    requestedBy = "/html/body/div[7]/div/section/div/article/div/div/div[2]/div[2]/form/div[1]/fieldset[2]/div[2]/div[2]/div/div/div/div/div[2]/div"

    requestedByDropdown = "/html/body/div[21]/div/div/div/div[2]/div/div[2]/span"

    FC = '#dropdownlistContentpurchase_order_defs_finareas_id > .jqx-combobox-input'

    deliveryTo = '#dropdownlistContentpurchase_order_defs_valshiptolocations_id > .jqx-combobox-input'

    addItems = '#formGrid0BulkSelectBtn > span'

    checkbox = '/html/body/div[7]/div/section/div/article/div/div/div[2]/div[2]/form/div[1]/fieldset[6]/div[4]/div/div/div/div[2]/div/div/div[2]/div/div[4]/div[2]/div/div[2]/div[1]/div/div[1]/div'

    addClose = '#modalAddCloseBulkSelect0'



    procurement() {
        cy.clickElement(this.procurementIcon)
    }

    purchaseOrder() {
        cy.clickElement(this.clickPurchaseOrder)
    }

    costingDetails1() {
        cy.xpath(this.customerJob).clear().type('C-1536 - PEPSI ')
        cy.xpathElement(this.customerJobDropdown, { timeout: 8000 }).wait(2000)
        cy.get(this.FC).should('have.value', data.inventoryLocation)
    }

    deliveryDetails() {
        cy.get(this.deliveryTo).should('have.value', data.deliveryTo)
        cy.wait(2000)
    }

    url() {
        cy.visitUrlWithQueryParam('/admin/forms/loadForm/139', '?openInTab=current');
    }

    verifyBtnPurchaseOrder() {
        cy.clickElement(this.btnPurchaseOrder)
    }

    purchaseOrderStatus() {
        cy.clickElement(this.currentStatus).wait(2000)
        cy.contains(data.currentStatus).click({ force: true })
        cy.wait(3000)
        cy.xpathElement(this.requestedBy)
        cy.contains(data.requestBy1).click({ force: true })
    }

    enterPurchase() {
        cy.xpathElement(this.supplier, { timeout: 8000 }).wait(2000)
        cy.xpathElement(this.supplierDropdown, { timeout: 8000 }).wait(2000)
    }

    costingDetails() {
        cy.xpath(this.customerJob).clear().type('C-1536 - PEPSI')
        cy.wait(4000)
        cy.xpathElement(this.customerJobDropdown, { timeout: 8000 })
        cy.wait(8000)

    }

    verifyAddItems() {
        cy.clickElement(this.addItems)
        cy.wait(3000)
        cy.xpathElement(this.checkbox)
        cy.wait(3000)
        cy.clickElement(this.addClose)
    }


}

export default new POrderPage();
