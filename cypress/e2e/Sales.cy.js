/// <reference types="Cypress" />

import LoginPage from "../page-objects/login-page.js"
import quotationPage from "../page-objects/quotation.js"
import SOPage from "../page-objects/sales-order.js"
import Payment from "../page-objects/receive-payment.js"
import dNotePage from "../page-objects/delivery-note.js"
import dReturnPage from "../page-objects/delivery-returns.js"

describe('HAL ERP', () => {
    Cypress.on('uncaught:exception', (err, runnable) => {
        // returning false here prevents Cypress from
        // failing the test
        return false
    })

    let cred;

    before(() => {
        cy.fixture('credentials').then(function (data) {
            cred = data;
        })
    })

    beforeEach(() => {
        LoginPage.visit()

        LoginPage.userNameInput
            .type(cred.user)

        LoginPage.passwordInput
            .type(cred.pass)

        LoginPage.loginButton
            .click()

        LoginPage.notification
            .dblclick()

        LoginPage.clickClose
            .click()

        LoginPage.sideOpen
            .click()

    })

    it.only('TestCase1 - Quotation', () => {

        quotationPage.sales()
        quotationPage.quotation()
        quotationPage.copy()
        quotationPage.customerDetails()

    })

    it('TestCase2 - SalesOrder', () => {

        quotationPage.sales()
        SOPage.salesOrder()
        SOPage.selectQuotation()

    })

    it('TestCase3 - DeliveryNote', () => {

        quotationPage.sales()
        dNotePage.deliveryNote()
        dNotePage.deliveryDetails()
        dNotePage.customerDetails()
        dNotePage.itemDetails()
        dNotePage.selectInventoryLocation()

    })

    it('TestCase4 - DeliveryReturns', () => {

        quotationPage.sales()
        dReturnPage.deliveryReturn()
        dReturnPage.selectDelivery()
        dReturnPage.selectItems()

    })

    it('TestCase5 - Payment', () => {

        Payment.clickIncome()
        Payment.selectCustomer()
        Payment.selectPaymentMethod()
        Payment.clickCheckbox()
        Payment.receivePayment()

    })
})
