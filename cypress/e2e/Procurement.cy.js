/// <reference types="Cypress" />

import LoginPage from "../page-objects/login-page"
import PRPage from "../page-objects/purchase-request.js"
import POrderPage from "../page-objects/purchase-order"
import PInvoicePage from "../page-objects/purchase-invoice.js"
import PReturnPage from "../page-objects/purchase-returns.js"
import ItemRPage from "../page-objects/item-receipts.js"
import PRFQPage from "../page-objects/purchase-rfq"
import BillPayments from "../page-objects/vendor-bills"


describe('HAL ERP', () => {
    Cypress.on('uncaught:exception', (err, runnable) => {
        // returning false here prevents Cypress from
        // failing the test
        return false
    })

    let cred;

    before(() => {
        cy.fixture('credentials').then(function (data) {
            cred = data;
        })
    })

    beforeEach(() => {
        LoginPage.visit()

        LoginPage.userNameInput
            .type(cred.user)

        LoginPage.passwordInput
            .type(cred.pass)

        LoginPage.loginButton
            .click()

        LoginPage.notification
            .dblclick()

        LoginPage.clickClose
            .click()

        LoginPage.sideOpen
            .click()

    })

    it('TestCase1 - PurchaseRequest', () => {

        PRPage.purchaseRequest()
        PRPage.verifyPurchaseRequestAdd()
        PRPage.createFromSalesOrder1()
        PRPage.purchaseRequester1()
        PRPage.purchaseRequests()

    })

    it('TestCase2 - PurchaseOrder', () => {

        POrderPage.procurement()
        POrderPage.purchaseOrder()
        POrderPage.enterPurchase()
        POrderPage.costingDetails1()
        POrderPage.deliveryDetails()
        POrderPage.verifyAddItems()

    })

    it('TestCase3 - PurchaseInvoice', () => {

        POrderPage.procurement()
        PInvoicePage.purchaseInvoice()
        PInvoicePage.selectSupplier()
        PInvoicePage.selectPurchase()

    })

    it.only('TestCase4 - PurchaseReturns', () => {

        POrderPage.procurement()
        PReturnPage.procurementModule()
        PReturnPage.selectTheTransaction()
        PReturnPage.returnDetails()
        PReturnPage.selectItems()

    })

    it('TestCase5 - ItemReceipts', () => {

        POrderPage.procurement()
        ItemRPage.itemReceipts()
        ItemRPage.selectPurchaseOrder()

    })

    it('TestCase6 - Payments', () => {

        BillPayments.clickPayment()
        BillPayments.selectAccountToPay()
        BillPayments.applyPayments()

    })

    it('TestCase7 - PurchaseRFQ', () => {

        PRFQPage.procurementModule()
        PRFQPage.createPurchaseRFQ()
        PRFQPage.purchaseRFQStatus()
        PRFQPage.enterSupplierDetails()
        PRFQPage.enterAddressDetails()
        PRFQPage.enterContactDetails()

    })
})
