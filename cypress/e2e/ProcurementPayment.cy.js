/// <reference types="Cypress" />

import LoginPage from "../page-objects/login-page"
import PRPage from "../page-objects/purchase-request.js"
import POrderPage from "../page-objects/purchase-order.js"
import PInvoicePage from "../page-objects/purchase-invoice.js"
import ItemRPage from "../page-objects/item-receipts"
import BillPayments from "../page-objects/vendor-bills"


describe('HAL ERP', { tags: ['@regression', '@sanity'] }, () => {
    Cypress.on('uncaught:exception', (err, runnable) => {
        // returning false here prevents Cypress from
        // failing the test
        return false
    })

    let cred;

    before(() => {
        cy.fixture('credentials').then(function (data) {
            cred = data;
        })
    })

    beforeEach(() => {
        LoginPage.visit()

        LoginPage.userNameInput
            .type(cred.user)

        LoginPage.passwordInput
            .type(cred.pass)

        LoginPage.loginButton
            .click()

        LoginPage.notification
            .dblclick()

        LoginPage.clickClose
            .click()

        LoginPage.sideOpen
            .click()

    })

    it('TestCase1 - PurchaseRequest-PurchaseOrder', { tags: ['PurchaseRequest', 'PurchaseOrder'] }, () => {
        Cypress.on('fail', (error, runnable) => {

        })

        PRPage.purchaseRequest()

        PRPage.url()

        PRPage.createFromSalesOrder()

        PRPage.purchaseRequests1()

        PRPage.costingDetails()

        PRPage.clickAdd()

        PRPage.verifyView()

        POrderPage.verifyBtnPurchaseOrder()

        POrderPage.purchaseOrderStatus()

        POrderPage.enterPurchase()

        POrderPage.costingDetails()

        PRPage.clickAdd()

        PRPage.verifyView()

    })

    describe('PurchaseOrder-ItemReceipt', { tags: ['@regression', '@smoke'] }, () => {

        it('TestCase2 - PurchaseOrder-ItemReceipt', { tags: ['PurchaseOrder', 'ItemReceipt'] }, () => {
            Cypress.on('fail', (error, runnable) => {

            })

            PRPage.purchaseRequest()

            POrderPage.url()

            POrderPage.purchaseOrderStatus()

            POrderPage.enterPurchase()

            POrderPage.costingDetails()

            POrderPage.verifyAddItems()

            PRPage.clickAdd()

            PRPage.verifyView()

            ItemRPage.verifyBtnItemReceipts()

        })
    })

    describe('ItemReceipt', { tags: ['@regression', '@smoke'] }, () => {

        it('TestCase3 - ItemReceipt', { tags: 'ItemReceipt' }, () => {
            Cypress.on('fail', (error, runnable) => {

            })

            PRPage.purchaseRequest()

            ItemRPage.url()

            ItemRPage.selectPurchaseOrder()

            PRPage.clickAdd()

            PRPage.verifyView()

        })
    })

    describe.only('PurchaseInvoice - Payments', { tags: ['@regression', '@sanity'] }, () => {

        it('TestCase4 - PurchaseInvoice - Payments', { tags: ['PurchaseInvoice', 'Payments'] }, () => {
            Cypress.on('fail', (error, runnable) => {

            })

            PRPage.purchaseRequest()

            PInvoicePage.url()

            PInvoicePage.selectSupplier()

            PInvoicePage.selectPurchase()

            PRPage.clickAdd()

            PRPage.verifyView()

            BillPayments.verifyPayment()

            BillPayments.selectAccountToPay()

            BillPayments.applyPayments()

            PRPage.clickAdd()

        })
    })

})
