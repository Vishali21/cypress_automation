import LoginPage from "../page-objects/login-page";

describe(`Login`, { tags: ['@regression', '@smoke'] }, () => {

    let cred;

    before(() => {
        cy.fixture('credentials').then(function (data) {
            cred = data;
        })
    })

    beforeEach(() => {
        LoginPage.visit();
    })

    it('Login with valid login credentials', { tags: ['Login', 'Valid'] }, () => {
        LoginPage.loginButton
            .should('be.visible')
            .and('be.enabled')
            .click()

        LoginPage.userNameInput
            .should('be.visible')
            .and('be.enabled')
            .type(cred.user)

        LoginPage.passwordInput
            .should('be.visible')
            .and('be.enabled')
            .type(cred.pass)

        LoginPage.loginButton
            .click()

        cy.url()
            .should('contain', '/admin/dashboard/home')

    })

    it(`Login with invalid username`, { tags: ['Login', 'Invalid'] }, () => {

        LoginPage.userNameInput
            .type(cred.invalidUser)

        LoginPage.passwordInput
            .type(cred.pass)

        LoginPage.loginButton
            .click()

        LoginPage.errorMessage
            .should('be.visible')
            .contains('These credentials do not match our records.')

    })


    it(`Login with invalid password`, { tags: ['Login', 'Invalid'] }, () => {

        LoginPage.userNameInput
            .type(cred.user)

        LoginPage.passwordInput
            .type(cred.invalidPassword)

        LoginPage.loginButton
            .click()

        LoginPage.errorMessage
            .should('be.visible')
            .contains('Undefined variable: sessionId (View: /var/www/halerp.com/public_html/halerp/live/app/views/user/login.blade.php)')

    })

})