/// <reference types="Cypress" />

import LoginPage from "../page-objects/login-page"
import PRPage from "../pom/purchase-request"
import POrderPage from "../pom/purchase-order.js"
import ItemRPage from "../pom/item-receipts.js"


describe('HAL ERP', () => {
    Cypress.on('uncaught:exception', (err, runnable) => {
        // returning false here prevents Cypress from
        // failing the test
        return false
    })

    let cred;

    before(() => {
        cy.fixture('credentials').then(function (data) {
            cred = data;
        })
    })

    beforeEach(() => {
        LoginPage.visit()

        LoginPage.userNameInput
            .type(cred.user)

        LoginPage.passwordInput
            .type(cred.pass)

        LoginPage.loginButton
            .click()

        LoginPage.notification
            .dblclick()

        LoginPage.clickClose
            .click()

        LoginPage.sideOpen
            .click()

    })

    it('TestCase1 - PurchaseOrder-ItemReceipt', () => {

        PRPage.purchaseRequest()

        cy.get('#PurchaseOrder > .nav-subitem > span').click()

        cy.get('#form_print_templatesBtnGrp9 > .fa').click({ force: true })

        cy.wait(3000)

        cy.xpath('/html/body/div[7]/div/section/div/article/div/div[2]/div/div[2]/div[2]/div[1]/div[1]/ul/li[2]/a').click({ force: true })
        cy.visit('https://issam.halerp.com/admin/forms/loadForm/139/450?openInTab=current')

        cy.get('#ItemReceipts_id').click()

    })

})
