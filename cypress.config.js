const { defineConfig } = require("cypress");

module.exports = defineConfig({

  // pageLoadTimeout: 120000,
  viewportWidth: 1700,
  viewportHeight: 900,
  defaultCommandTimeout: 10000,
  // requestTimeout:5000,
  // responseTimeout:30000,
  waitForAnimations: false,
  animationDistanceThreshold: 50,


  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
      require('cypress-mochawesome-reporter/plugin')(on);
      require('@cypress/grep/src/plugin')
      return config;
    },


    // Mochawesome html report 
    reporter: "cypress-mochawesome-reporter",
    reporterOptions: {
      "reportDir": "reports/mochawesome",
      "charts": true,
      "embeddedScreenshots": true,
      "inlineAssets": true,
      "overwrite": true,
      "html": true,
      "json": false,
      "reportTitle": "HAL ERP",
      "reportPageTitle": "Cypress Automation Result",
      "code": true,
      "timestamp": true
    },


    // video reporting
    video: true,
    videoUploadOnPasses: false,
    videosFolder: 'reports/video',


    // screenshots reporting
    screenshotOnRunFailure: true,
    screenshotsFolder: 'reports/screenshots',


    // base url of the application
    baseUrl: 'https://issam.halerp.com',

  },
  
  
  

});
